<?php

return [
    'admin' => [
        'login' => [
            'account' => [
                'required' => '請確實填寫帳號',
                'min' => '帳號請勿低於4個字',
                'max' => '帳號請勿超過16個字',
                'regex' => '帳號請勿輸入中文、符號!',
            ],
            'password' => [
                'required' => '請確實填寫密碼',
                'min' => '密碼請勿低於6個字',
                'max' => '密碼請勿超過16個字',
                'regex' => '密碼請勿輸入中文或特殊符號',
            ],
        ],
        'base' => [
            'name' => [
                'required' => '請確實填寫暱稱',
                'max' => '暱稱請勿超過100個字',
            ],
            'new_password' => [
                'required' => '請確實填寫新密碼',
                'min' => '新密碼請勿低於6個字',
                'max' => '新密碼請勿超過16個字',
                'regex' => '新密碼請勿輸入中文或特殊符號',
            ],
            'check_password' => [
                'required' => '請確實填寫確認密碼',
                'min' => '確認密碼請勿低於6個字',
                'max' => '確認密碼請勿超過16個字',
                'regex' => '確認密碼請勿輸入中文或特殊符號',
            ],
        ],
        'system' => [
            'driver' => [
                'required' => '請確實填寫信件驅動',
                'max' => '信件驅動請勿超過10個字',
            ],
            'host' => [
                'required' => '請確實填寫信件商服務端',
                'regex' => '信件商服務端請勿輸入中文',
                'max' => '信件商服務端請勿超過30個字',
            ],
            'port' => [
                'required' => '請確實填寫埠',
                'integer' => '埠必須為整數',
                'max' => '埠請勿超過5個字',
            ],
            'user_name' => [
                'required' => '請確實填寫寄件者稱呼',
                'max' => '寄件者稱呼請勿超過30個字',
                'email' => '請輸入正確的寄件者稱呼格式',
            ],
            'password' => [
                'required' => '請確實填寫密碼',
                'max' => '密碼請勿超過16個字',
                'regex' => '密碼請勿輸入中文或特殊符號',
            ],
            'encryption' => [
                'required' => '請確實填寫加密',
                'regex' => '加密請勿輸入中文或特殊符號',
                'max' => '加密請勿超過30個字',
            ],
            'sender_email' => [
                'required' => '請確實填寫寄件者電子郵件',
                'max' => '寄件者電子郵件請勿超過30個字',
                'email' => '請輸入正確的寄件者電子郵件格式',
            ],
            'sender_name' => [
                'required' => '請確實填寫寄件者名稱',
                'max' => '寄件者名稱請勿超過30個字',
            ],
            'msg_url' => [
                'required' => '請確實填寫簡訊API',
                'max' => '簡訊API名稱請勿超過100個字',
            ],
            'msg_valet' => [
                'required' => '請確實填寫代客下單模板',
                'max' => '代客下單模板請勿超過7個字',
            ],
            'msg_agent' => [
                'required' => '請確實填寫店舖模板模板',
                'max' => '店舖模板模板請勿超過7個字',
            ],
            'pay_url' => [
                'required' => '請確實填寫金流API',
                'max' => '金流API請勿超過100個字',
            ],
            'pay_key' => [
                'required' => '請確實填寫金流金鑰',
                'max' => '金流金鑰請勿超過50個字',
            ],
            'vendor_stop' => [
                'required' => '請確實填寫商戶開關',
            ],
            'manager_stop' => [
                'required' => '請確實填寫代理商開關',
            ],
            'agent_stop' => [
                'required' => '請確實填寫店舖開關',
            ],
            'email_status' => [
                'required' => '請確實填寫寄信開關',
            ],
            'msg_status' => [
                'required' => '請確實填寫簡訊開關',
            ],
            'phone' => [
                'required' => '請確實填寫電話',
                'max' => '電話請勿超過30個字',
                'regex' => '電話只可輸入數字',
            ],
            'email' => [
                'required' => '請確實填寫收件者電子郵件',
                'max' => '收件者電子郵件請勿超過30個字',
                'email' => '請輸入正確的收件者電子郵件格式',
            ],
        ],
    ],
];
