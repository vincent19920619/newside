<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\LangController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\CommonController;
use App\Http\Controllers\Admin\BaseController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('lang/change', [LangController::class, 'change'])->name('Admin.change.lang');
Route::group(['middleware'=>['admin.lang']], function(){
    // 登入
    Route::get('/', [AuthController::class, 'index'])->name('Admin.auth');
    Route::get('logout', [AuthController::class, 'logout'])->name('Admin.auth.logout');
    Route::post('check', [AuthController::class, 'check'])->name('Admin.auth.check');

    Route::group(['middleware'=>['admin.auth']], function(){
        // 主頁(master)
        Route::get('index', [CommonController::class, 'index'])->name('Admin.index');
        Route::post('index/show', [CommonController::class, 'show'])->name('Admin.index.show');
        // 帳戶設定
        Route::get('base', [BaseController::class, 'index'])->name('Admin.base');
        Route::post('base/show', [BaseController::class, 'show'])->name('Admin.base.show');
        Route::post('base/update', [BaseController::class, 'update'])->name('Admin.base.update');
        Route::post('base/password', [BaseController::class, 'password'])->name('Admin.base.password');
    });
});
// ckfinder
Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('ckeditor.image-upload');