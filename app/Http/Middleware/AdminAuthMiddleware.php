<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $AuthData = Auth::guard('admin')->check(); 

        if ($AuthData === false) {
            Auth::logout();
            Session::flush();

            return redirect()->route("Admin.auth");
        } else {
            return $next($request);
        }
    }
}
