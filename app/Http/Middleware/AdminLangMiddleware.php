<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Request;

class AdminLangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (is_null(session()->get('admin_locale'))) {
            session()->put('admin_locale', 'tw');
            App::setLocale('tw');
        } else {
            $locale = session()->get('admin_locale');
            App::setLocale($locale);
        }

        return $next($request);
    }
}
