<?php

namespace App\Http\Controllers\Admin;

use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Models\Menu;

class ViewController extends Controller
{
    function __construct(){
        $this->menu = new Menu();
    }

    public function rander($view, $TPL = [])
    {
        $AuthData = Auth::guard('admin')->user();
        $aView['TPL'] = $TPL;
        $aView['BASE_TPL'] = [];
        $aView['NAME'] = $AuthData['name'];
        $group = $this->menu->getGroup(1);
        $menu = $this->menu->getList(1);
        $currentURL = $_SERVER["REQUEST_URI"];
        $aView['currentURL'] = $currentURL;

        foreach ($group as $group_key => $group_val) {
            $aView['BASE_TPL'][] = $group_val;
            $aView['BASE_TPL'][$group_key]['active'] = '';
            $aView['BASE_TPL'][$group_key]['new_name'] = $group_val['name'];

            if (strpos($currentURL, $group_val['link']) !== false && $group_val['link'] != '') {
                $aView['BASE_TPL'][$group_key]['active'] = ' active open';
            }

            foreach ($menu as $menu_key => $menu_val) {

                // 作品
                if ($menu_val['menu_id'] == 4 && $group_val['id'] == $menu_val['menu_id']) {
                    $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']] = $menu_val;
                    $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']]['active'] = '';

                    if (strpos($currentURL, $menu_val['link']) !== false) {
                        $aView['BASE_TPL'][$group_key]['active'] = ' active open';
                        $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']]['active'] = 'active';
                    }
                }

                // 文章
                if ($menu_val['menu_id'] == 5 && $group_val['id'] == $menu_val['menu_id']) {
                    $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']] = $menu_val;
                    $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']]['active'] = '';

                    if (strpos($currentURL, $menu_val['link']) !== false) {
                        $aView['BASE_TPL'][$group_key]['active'] = ' active open';
                        $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']]['active'] = 'active';
                    }
                }

                // 基本設定
                if ($menu_val['menu_id'] == 6 && $group_val['id'] == $menu_val['menu_id']) {
                    $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']] = $menu_val;
                    $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']]['active'] = '';

                    if (strpos($currentURL, $menu_val['link']) !== false) {
                        $aView['BASE_TPL'][$group_key]['active'] = ' active open';
                        $aView['BASE_TPL'][$group_key]['menu_list'][$menu_val['sort']]['active'] = 'active';
                    }
                }
            }
        }

        return View::make($view, $aView);
    }
}
