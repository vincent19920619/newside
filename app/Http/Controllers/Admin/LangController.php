<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App;

class LangController extends ViewController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function change(Request $request)
    {
        // App::setLocale($request->lang);
        session()->put('admin_locale', $request->lang);
        // echo \Lang::get('vendor.manager.table.status_4');
        // exit;

        return redirect()->back();
    }
}