<?php

namespace App\Http\Controllers\Admin;

use Log;
use Lang;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\ViewController;
use App\Repositories\{AdminRepository};

class BaseController extends ViewController
{
    function __construct(AdminRepository $AdminRepository){
        parent::__construct();
        $this->AdminRepository = $AdminRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->rander("admin.base");
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {        
        $AuthData = Auth::guard('admin')->user();
        $data = [];
        $admin_data = [];
        $admin_data = $this->AdminRepository->getAdminData($AuthData['id']);
        $data['name'] = $admin_data['name'];
        $data['account'] = $admin_data['account'];

        return response()->json([ 'configData' => $data ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $AuthData = Auth::guard('admin')->user();
        $input = $request->all();
        // 資料檢查
        $check_val = Validator::make( $input , [
            'name' => 'required|max:100',
        ] , [
            'name.required' => Lang::get('validation.admin.base.name.required'),
            'name.max'      => Lang::get('validation.admin.base.name.max'),
        ]);

        if ($check_val->fails()){
            $error = $check_val->errors()->first();

            return response()->json(['type' => false, 'message' => $error]);
        }
        // 取得舊資料
        $admin_data = [];
        $admin_data = $this->AdminRepository->getAdminData($AuthData['id']);
        // 更新資料
        $this->AdminRepository->updateAdminData($AuthData['id'], [
            'name' => $input['name']
        ]);
        // log
        $previous = json_encode(['id' => $AuthData['id'], 'name' => $admin_data['name']]);
        $next     = json_encode(['id' => $AuthData['id'], 'name' => $input['name']]);
        Log::channel('admin')->info("message => 帳戶設定", ['previous' => $previous, 'next' => $next]);

        return response()->json(['type' => true, 'message' => Lang::get('admin.base.controller.msg_1')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function password(Request $request)
    {
        $AuthData = Auth::guard('admin')->user();
        $input = $request->all();
        // 資料檢查
        $check_val = Validator::make( $input , [
            'new_password'   => 'required|min:6|max:16|regex:/^[a-zA-Z0-9!@#$%^&*()_+}{]*$/',
            'check_password' => 'required|min:6|max:16|regex:/^[a-zA-Z0-9!@#$%^&*()_+}{]*$/',
        ] , [
            'new_password.required'   => Lang::get('validation.admin.base.new_password.required'),
            'new_password.regex'      => Lang::get('validation.admin.base.new_password.regex'),
            'new_password.min'        => Lang::get('validation.admin.base.new_password.min'),
            'new_password.max'        => Lang::get('validation.admin.base.new_password.max'),
            'check_password.required' => Lang::get('validation.admin.base.check_password.required'),
            'check_password.regex'    => Lang::get('validation.admin.base.check_password.regex'),
            'check_password.min'      => Lang::get('validation.admin.base.check_password.min'),
            'check_password.max'      => Lang::get('validation.admin.base.check_password.max'),
        ]);

        if ($check_val->fails()){
            $error = $check_val->errors()->first();

            return response()->json(['type' => false, 'message' => $error]);
        }

        if ($input['new_password'] != $input['check_password']) {

            return response()->json(['type' => false, 'message' => Lang::get('admin.base.controller.msg_2')]);
        }
        // check password
        $admin_data = [];
        $admin_data = $this->AdminRepository->getAdminData($AuthData['id']);

        if (Hash::check($input['new_password'], $admin_data['password']) == true) {

            return response()->json(['type' => false, 'message' => Lang::get('admin.base.controller.msg_3')]);
        }
        // 更新密碼
        $this->AdminRepository->updatePassword($AuthData['id'], [
            'passwd_view' => $input['new_password'], 
            'password'    => Hash::make($input['new_password'])
        ]);
        //log
        $previous = json_encode(['id' => $admin_data['id'], 'password' => $admin_data['passwd_view']]);
        $next     = json_encode(['id' => $admin_data['id'], 'new_password' => $input['new_password'], 'check_password' => $input['check_password']]);
        Log::channel('admin')->info("message => 修改密碼", ['previous' => $previous, 'next' => $next]);

        return response()->json(['type' => true, 'message' => Lang::get('admin.base.controller.msg_4')]);
    }
}
