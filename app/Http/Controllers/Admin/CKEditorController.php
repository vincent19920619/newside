<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CKEditorController extends Controller
{
    public function upload(Request $request)
    {
        $mimetype = false;

        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
            $request->file('upload')->move(public_path('images'), $fileName);
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimetype = finfo_file($finfo, './images/'.$fileName);

            if($mimetype == 'image/png' || $mimetype == 'image/jpg' || $mimetype == 'image/jpeg' || $mimetype == 'image/svg'){
                $msg = '圖片上傳成功!';
            } else {
                $msg = '上傳檔案非圖片！';
                File::delete('./images/'.$fileName);
            }
            // $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // @header('Content-type: text/html; charset=utf-8');
            // echo $response;
            return response()->json(['uploaded' => 1, 'fileName' => $fileName, 'url' => $url]);
        }
    }
}
