<?php

namespace App\Http\Controllers\Admin;

use Lang;
use Validator;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Repositories\{AdminRepository, AdminLogRepository};

class AuthController extends Controller
{
    /**
     * 建構
     */
    public function __construct(AdminRepository $AdminRepository, AdminLogRepository $AdminLogRepository) {
        $this->AdminRepository    = $AdminRepository;
        $this->AdminLogRepository = $AdminLogRepository;
    }

    public function index()
    {
        return View::make('admin.login');
    }
    
    public function check(Request $request)
    {
        $input = $request->all();
        // 資料檢查
        $check_val = Validator::make( $input , [
            'account'     => 'required|min:4|max:16|regex:/^[a-zA-Z0-9]*$/',
            'password'    => 'required|min:6|max:16|regex:/^[a-zA-Z0-9!@#$%^&*()_+}{]*$/',
            // 'g2fa' => 'required'
        ] , [
            'account.required'  => Lang::get('validation.admin.login.account.required'),
            'account.min'       => Lang::get('validation.admin.login.account.min'),
            'account.max'       => Lang::get('validation.admin.login.account.max'),
            'account.regex'     => Lang::get('validation.admin.login.account.regex'),
            'password.required' => Lang::get('validation.admin.login.password.required'),
            'password.regex'    => Lang::get('validation.admin.login.password.regex'),
            'password.min'      => Lang::get('validation.admin.login.password.min'),
            'password.max'      => Lang::get('validation.admin.login.password.max'),
        ]);

        if ($check_val->fails()) {
            $error = $check_val->errors()->first();

            return response()->json(['type' => false, 'message' => $error]);
        }
        // 密碼Hash
        $input['passwd_hash'] = Hash::make($input['password']);
        // 搜尋驗證資料
        $admin_data = $this->AdminRepository->checkLoginData($input['account']);

        if ($admin_data == '') {

            return response()->json(['type' => false, 'message' => Lang::get('admin.login.controller.msg_1')]);
        }

        // 密碼比對
        if (Hash::check($input['password'], $admin_data['password']) == false) {

            return response()->json(['type' => false, 'message' => Lang::get('admin.login.controller.msg_2')]);
        }
        // 更新token
        $this->AdminRepository->updateRememberToken($admin_data['id'], $input['_token']);
        // 寫入登入log
        $this->AdminLogRepository->insertLog($admin_data['id'], $request->ip());
        // 驗證登入資訊
        $attempt = Auth::guard('admin')->attempt([
            'account' => $input['account'],
            'password' => $input['password'],
        ]);

        if ($attempt === false) {

            return response()->json(['type' => false, 'message' => Lang::get('admin.login.controller.msg_3')]);
        }

        return response()->json(['type' => true, 'message' => Lang::get('admin.login.controller.msg_4') ]);
    }
    
    public function logout()
    {
        Auth::logout();
        Session::flush();

        return redirect()->route("Admin.auth");
    }
}
