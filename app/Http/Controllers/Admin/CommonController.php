<?php

namespace App\Http\Controllers\Admin;

use Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\ViewController;
use App\Repositories\{AdminRepository, UserRepository};

class CommonController extends ViewController
{
    /**
     * 建構
     */
    public function __construct(AdminRepository $AdminRepository, UserRepository $UserRepository) {
        parent::__construct();
        $this->AdminRepository = $AdminRepository;
        $this->UserRepository  = $UserRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->rander("admin.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        // 管理員人數
        $admin = $this->AdminRepository->countAdminCount();
        $user = $this->UserRepository->countUserCount();
        $message = 0;
        $comment = 0;

        return response()->json([ 'member' => $admin, 'user' => $user, 'message' => $message, 'comment' => $comment ]);
    }
}
