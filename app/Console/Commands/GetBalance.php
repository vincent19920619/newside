<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GetBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'getbalance:getbalance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fullNode = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');
        $solidityNode = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');
        $eventServer = new \IEXBase\TronAPI\Provider\HttpProvider('https://api.trongrid.io');

        try {
            $tron = new \IEXBase\TronAPI\Tron($fullNode, $solidityNode, $eventServer);
        } catch (\IEXBase\TronAPI\Exception\TronException $e) {
            exit($e->getMessage());
        }
        $agent_trx1 = new \App\Models\Admin\AgentTrx();
        $agent_trxs = [];
        $privateKey = '';
        $agent_trxs = $agent_trx1->getAgentTrxData();

        foreach ($agent_trxs as $key => $val) {
            $tron->setAddress($val['address_base58']);
            $balance = $tron->getBalance(null, true);

            if ($balance != 0) {
                \DB::beginTransaction();

                try {
                    $agent_trx2 = new \App\Models\Admin\AgentTrx();
                    $agent_trx2->updateAgentTrx($val['id'], $balance);
                    $transfer = $tron->accountsTransactions($val['address_base58']);
                    $a = json_decode($transfer, true);
                    $b = [];
                    $b = $a['data'];
                    $c = [];

                    foreach ($b as $key1 => $val1) {
                        $trx_transaction1 = new \App\Models\Admin\TrxTransaction();
                        $trx_transaction = [];
                        $trx_transaction = $trx_transaction1->getTrxTransaction($val1['txID']);

                        if ($trx_transaction == '') {

                            if ($val1['raw_data']['contract'][0]['type'] == 'TransferContract') {

                                if ($val1['raw_data']['contract'][0]['parameter']['value']['owner_address'] == $val['address_hex'] ) {
                                    $c[$key1]['agent_id'] = $val['agent_id'];
                                } elseif ($val1['raw_data']['contract'][0]['parameter']['value']['to_address'] == $val['address_hex'] ) {
                                    $c[$key1]['agent_id'] = $val['agent_id'];
                                }
                                $c[$key1]['txID']               = $val1['txID'];
                                $c[$key1]['contractRet']        = $val1['ret'][0]['contractRet'];
                                $c[$key1]['fee']                = $val1['ret'][0]['fee'];
                                $c[$key1]['signature']          = $val1['signature'][0];
                                $c[$key1]['net_usage']          = $val1['net_usage'];
                                $c[$key1]['raw_data_hex']       = $val1['raw_data_hex'];
                                $c[$key1]['net_fee']            = $val1['net_fee'];
                                $c[$key1]['energy_usage']       = $val1['energy_usage'];
                                $c[$key1]['blockNumber']        = $val1['blockNumber'];
                                $c[$key1]['block_timestamp']    = $val1['block_timestamp'];
                                $c[$key1]['energy_fee']         = $val1['energy_fee'];
                                $c[$key1]['energy_usage_total'] = $val1['energy_usage_total'];
                                $c[$key1]['amount']             = $val1['raw_data']['contract'][0]['parameter']['value']['amount'];
                                $c[$key1]['owner_address']      = $val1['raw_data']['contract'][0]['parameter']['value']['owner_address'];
                                $c[$key1]['to_address']         = $val1['raw_data']['contract'][0]['parameter']['value']['to_address'];
                            }
                        }
                    }

                    foreach ($c as $key2 => $val2) {
                        $trx_transaction2 = new \App\Models\Admin\TrxTransaction();
                        $trx_transaction2->createTrxTransaction($val2);
                    }
                    \DB::commit();
                } catch(\Illuminate\Database\QueryException $ex) {
                    \DB::rollback();
                    $msg['core_msg']  = '操作錯誤。';
    
                    \Log::info("Command => ".$msg['core_msg']);
                }
            }
        }
    }
}
