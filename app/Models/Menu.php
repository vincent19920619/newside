<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menu";

    public function getGroup($type)
    {
        $arrData = [];
        $arrData = $this->select('*')
                        ->where('type', $type)
                        ->where('menu_id', 0)
                        ->where('status', '!=', 2)
                        ->orderBy('sort', 'asc')
                        ->get();

        return ($arrData == null) ? '': $arrData->toArray();
    }

    public function getList($type)
    {
        $arrData = [];
        $arrData = $this->select('*')
                        ->where('type', $type)
                        ->where('menu_id', '!=', 0)
                        ->where('status', '!=', 2)
                        ->orderBy('sort', 'asc')
                        ->get();

        return ($arrData == null) ? '': $arrData->toArray();
    }
}
