<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $table = "system";

    public function getSystem($id = '')
    {
        $arrData = [];
        $arrData = $this->select('id', 'driver', 'host', 'port', 'user_name', 'password', 'encryption', 'sender_email', 'sender_name')
                    ->where('id', 1)
                    ->first();

        return ($arrData == null) ? '': $arrData->toArray();
    }

    public function updateSystem($data = [])
    {
        $updateData['driver']       = $data['driver'];
        $updateData['host']         = $data['host'];
        $updateData['port']         = $data['port'];
        $updateData['user_name']    = $data['user_name'];
        $updateData['password']     = $data['password'];
        $updateData['encryption']   = $data['encryption'];
        $updateData['sender_email'] = $data['sender_email'];
        $updateData['sender_name']  = $data['sender_name'];
        $updateData['admin_stop']   = $data['admin_stop'];
        $updateData['email_status'] = $data['email_status'];
        $updateData['msg_status']   = $data['msg_status'];
        $updateData['updated_at']   = date("Y-m-d H:i:s");
        $result = $this->where('id', $data['id'])
                        ->lockForUpdate()
                        ->update($updateData);

        return $result;
    }

    public function getSystemContent($id = '')
    {
        $arrData = [];
        $arrData = $this->select('id', 'valet_content', 'agent_content', 'email_status')
                    ->where('id', 1)
                    ->first();

        return ($arrData == null) ? '': $arrData->toArray();
    }

    public function updateSystemValetContent($data = [])
    {
        $updateData['valet_content'] = $data['valet_content'];
        $updateData['updated_at']    = date("Y-m-d H:i:s");
        $result = $this->where('id', $data['id'])
                        ->lockForUpdate()
                        ->update($updateData);

        return $result;
    }

    public function updateSystemAgentContent($data = [])
    {
        $updateData['agent_content'] = $data['agent_content'];
        $updateData['updated_at']    = date("Y-m-d H:i:s");
        $result = $this->where('id', $data['id'])
                        ->lockForUpdate()
                        ->update($updateData);

        return $result;
    }

    public function getSystemData($id = '')
    {
        $arrData = [];
        $arrData = $this->select('id', 'valet_content', 'agent_content', 'email_status', 'msg_status', 'msg_url', 'msg_valet', 'msg_agent', 'pay_url', 'pay_key')
                    ->where('id', 1)
                    ->first();

        return ($arrData == null) ? '': $arrData->toArray();
    }
}
