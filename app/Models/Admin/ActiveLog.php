<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class ActiveLog extends Model
{
    protected $table = "admin_active_log";

    public function createActiveLog($data)
    {

        $this->admin_id = $data['admin_id'];
        $this->page     = $data['page'];
        $this->page_id  = $data['page_id'];
        $this->action   = $data['action'];

        if (!empty($data['previous']) && !empty($data['next'])) {
            $this->previous = $data['previous'];
            $this->next     = $data['next'];
        }
        $this->created_at = date("Y-m-d H:i:s");
        $this->save();
    }
}
