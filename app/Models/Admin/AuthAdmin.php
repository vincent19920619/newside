<?php

namespace App\Models\Admin;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;

class AuthAdmin extends Authenticatable
{
    protected $table='admin';
    // 默認id Join
    use Notifiable;
    
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('admin', function(Builder $builder) {
            $builder->select( ['admin.*'] );
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
        
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
