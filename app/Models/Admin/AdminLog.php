<?php

namespace App\Models\Admin;

use DB;
use Illuminate\Database\Eloquent\Model;

class AdminLog extends Model
{
    protected $table = "admin_log";
    protected $fillable = [];
    protected $guarded = [];
    protected $hidden = [];
    protected $casts = [];
    public $timestamps = true;
}
