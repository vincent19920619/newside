<?php

namespace App\Repositories;

use App\Models\Admin\Admin;

class AdminRepository
{
    /**
     * 找尋管理員帳密
     */
    public static function checkLoginData(string $account)
    {
        return Admin::where('account', $account)->where('status', 'on')->first();
    }

    /**
     * 更新管理員token
     */
    public static function updateRememberToken(int $id, string $remember_token)
    {
        return Admin::where('id', $id)->update(['remember_token' => $remember_token]);
    }

    /**
     * 計算管理員人數
     */
    public static function countAdminCount()
    {
        return Admin::count();
    }

    /**
     * 取得單一資料
     */
    public static function getAdminData(int $id)
    {
        return Admin::where('id', $id)->first();
    }

    /**
     * 更新管理員資料
     */
    public static function updateAdminData(int $id, array $data)
    {
        return Admin::lockForUpdate()->where('id', $id)->update($data);
    }

    /**
     * 更新管理員密碼
     */
    public static function updatePassword(int $id, array $data)
    {
        return Admin::lockForUpdate()->where('id', $id)->update($data);
    }
}
