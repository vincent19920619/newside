<?php

namespace App\Repositories;

use App\Models\Admin\User;

class UserRepository
{
    /**
     * 計算用戶人數
     */
    public static function countUserCount()
    {
        return User::count();
    }
}
