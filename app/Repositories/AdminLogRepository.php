<?php

namespace App\Repositories;

use App\Models\Admin\AdminLog;

class AdminLogRepository
{
    /**
     * 寫入登入紀錄
     */
    public static function insertLog(int $admin_id, string $ip)
    {
        return AdminLog::insert([
            'admin_id' => $admin_id,
            'ip' => $ip
        ]);
    }
}
