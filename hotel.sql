-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- 主機： localhost
-- 產生時間： 2024 年 01 月 17 日 12:01
-- 伺服器版本： 5.6.50-log
-- PHP 版本： 8.1.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `hotel`
--

-- --------------------------------------------------------

--
-- 資料表結構 `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwd_view` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'on:開啟 off:關閉 delete:刪除',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin`
--

INSERT INTO `admin` (`id`, `name`, `account`, `passwd_view`, `password`, `remember_token`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '管理員1', 'admin', '123456', '$2y$10$BD69fegx401lL81h74pAQe8mFY5Y9j4UgSb/yZOr0Y6YvCTmV2ypq', '8F5z6g936L45u5yQuWgFrWUiNQdY7fbgmyH6UzzT', 'on', NULL, '2022-07-08 11:10:31', '2024-01-17 11:58:35');

-- --------------------------------------------------------

--
-- 資料表結構 `admin_active_log`
--

CREATE TABLE `admin_active_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `page` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_id` int(11) NOT NULL,
  `action` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `previous` text COLLATE utf8mb4_unicode_ci,
  `next` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `admin_log`
--

CREATE TABLE `admin_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `admin_log`
--

INSERT INTO `admin_log` (`id`, `admin_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 1, '172.17.0.1', NULL, NULL),
(2, 1, '172.17.0.1', NULL, NULL),
(3, 1, '172.17.0.1', NULL, NULL),
(4, 1, '172.17.0.1', NULL, NULL),
(5, 1, '172.17.0.1', NULL, NULL),
(6, 1, '172.17.0.1', NULL, NULL),
(7, 1, '172.17.0.1', NULL, NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `commentator` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '評論人帳號',
  `num` int(11) NOT NULL COMMENT '評分',
  `content` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '評論內容',
  `status` int(11) NOT NULL COMMENT '1:審核通過 2:審核不通過 3:刪除 4:前台新增 尚未審核	',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1:管理者',
  `menu_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1:開啟 0:關閉 3:分頁',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `menu`
--

INSERT INTO `menu` (`id`, `name`, `link`, `link_title`, `icon`, `type`, `menu_id`, `sort`, `admin_id`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '總覽', '/admin/index', NULL, '', 1, 0, 1, 1, 1, NULL, '2022-01-20 03:45:39', NULL);

-- --------------------------------------------------------

--
-- 資料表結構 `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `system`
--

CREATE TABLE `system` (
  `id` int(11) NOT NULL,
  `driver` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `host` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port` int(11) NOT NULL,
  `user_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `encryption` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `system`
--

INSERT INTO `system` (`id`, `driver`, `host`, `port`, `user_name`, `password`, `encryption`, `sender_email`, `sender_name`, `created_at`, `updated_at`) VALUES
(1, 'smtp', 'smtp.googlemail.com', 465, 'vincent0619@gmail.com', 'zxc19920619', 'ssl', 'vincent0619@gmail.com', '網站測試', '2022-08-08 18:56:14', '2022-09-01 11:15:56');

-- --------------------------------------------------------

--
-- 資料表結構 `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passwd_view` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'on:開啟 off:關閉 delete:刪除',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 傾印資料表的資料 `user`
--

INSERT INTO `user` (`id`, `name`, `account`, `passwd_view`, `password`, `remember_token`, `phone`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '會員1', 'member1', '252551', '$2y$10$524VMO.J/exidHIbnaC1X.5wAhgtfRV/WZcK6SvT2maKApVJq2O/.', '8cHmeHrrv7P5GQKfeZSxjEfyKX6S90JpKLg53z0gLCP4kiPSYCFYtliIPNMV', '09876543211', 'on', NULL, '2022-08-03 15:37:39', '2022-09-13 18:09:32'),
(2, '會員2', 'member2', '123456', '$2y$10$OqgRnFfcAobB6FZPwbQJbeoonac4mHpvPAY3JPXoZ73l8eiGLcZxG', '77mVRpT48d5Uu4x16IbMZQrh06ckeiUfHb30vqfOd8ANKT88OEsVYDGawJTG', '09876543212', 'on', NULL, '2022-08-03 15:39:28', '2022-10-14 09:46:12'),
(3, '會員3', 'member3', '123456', '$2y$10$1JLLa3e4LTYMYjsp1UomQeNiwNhaRNPn0iFwf.tv/cDkyTtsv0Elq', 'hY0iC43v2iWWlQLYzTsKWNhO8cKw9vMSx7oAHhWo', '09876543213', 'on', NULL, '2022-08-03 15:40:49', '2022-10-14 09:42:12'),
(4, '會員4', 'member4', '123456', '$2y$10$atgMbs/DejhYAQTl3mDvb.mYLsHQtypYf2z3UnLoOG56bUQzInmXi', '0BeIyZJxTgHn6adwEf8ozETxuk0wbdydSHgSqogm', '09876543214', 'on', NULL, '2022-08-23 11:36:58', '2022-10-14 10:08:40'),
(5, '會員5', 'member5', '123456', '$2y$10$pnGwaZGbhnJUfeNYK.vN8eaKkQ5V1quh2myba63S4Mfj5FKMLqBiy', 'H75ilyRclx0DxQY6dWJBcizJdzAg8B7STLZXVbgK', '09876543215', 'on', NULL, '2022-08-23 14:13:41', '2022-09-13 19:08:29'),
(6, '會員6', 'member6', '123456', '$2y$10$sb0RrDQyfDjqz2Y3d3vi4.EUBqLMbhpurr1qF5NKvwiBf3ImLrNmq', 'I1dGGVtaDMG69CmivZIY9SaMeJFB1556JSkuxOk0', '09876543216', 'on', NULL, '2022-08-23 15:07:55', '2022-10-12 14:01:58');

-- --------------------------------------------------------

--
-- 資料表結構 `user_active_log`
--

CREATE TABLE `user_active_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `u_id` int(11) NOT NULL,
  `page` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_id` int(11) NOT NULL,
  `action` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_account` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous` text COLLATE utf8mb4_unicode_ci,
  `next` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `user_log`
--

CREATE TABLE `user_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `u_id` int(11) NOT NULL,
  `ip` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 資料表結構 `user_picture`
--

CREATE TABLE `user_picture` (
  `id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL COMMENT 'user id',
  `pic` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `admin_active_log`
--
ALTER TABLE `admin_active_log`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `admin_log`
--
ALTER TABLE `admin_log`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `system`
--
ALTER TABLE `system`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user_active_log`
--
ALTER TABLE `user_active_log`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user_log`
--
ALTER TABLE `user_log`
  ADD PRIMARY KEY (`id`);

--
-- 資料表索引 `user_picture`
--
ALTER TABLE `user_picture`
  ADD PRIMARY KEY (`id`);

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `admin_active_log`
--
ALTER TABLE `admin_active_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `admin_log`
--
ALTER TABLE `admin_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `system`
--
ALTER TABLE `system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `user_active_log`
--
ALTER TABLE `user_active_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `user_log`
--
ALTER TABLE `user_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `user_picture`
--
ALTER TABLE `user_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
