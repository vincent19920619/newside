@extends('admin.layouts.master')
@section('nav')
<h5 class="nav-header">{{ __('admin.base.title') }}</h5>
@endsection
@section('content')
<div class="content-wrapper" id="app" v-cloak>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-xxl">
                <div class="card mb-4" style="padding-left: 10%">
                    <div class="card-body" style="padding-left: 10%">
                        <form>
                            <div class="row mb-3">
                                <div class="col-sm-10">
                                    <label class="col-sm-3 text-right control-label col-form-label">{{ __('admin.base.name') }}</label>
                                    <input type="text" class="form-control" required="" v-model="edit_data.name"/>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-10">
                                    <label for="account" class="col-sm-3 text-right control-label col-form-label">{{ __('admin.base.account') }}</label>
                                    <input type="text" class="form-control" required="" id="account" name="account" v-model="edit_data.account" readonly unselectable="on">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-sm-10">
                                    {{-- <label for="account" class="col-sm-3 text-right control-label col-form-label"></label> --}}
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#passwordModal" @click="passwordData">
                                        {{ __('admin.base.edit_password') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="from-button">
                                    <button id="btn" type="button" class="btn btn-success" @click="updateData()" :disabled="isDisabled">
                                        {{ __('admin.base.submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-backdrop fade"></div>
    <div class="modal fade" id="passwordModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">{{ __('admin.base.edit_password') }}</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <label for="new_password" class="col-form-label">{{ __('admin.base.new_password') }}:</label>
                            <input type="password" class="form-control" v-model="password_data.new_password">
                        </div>
                        <div class="mb-3">
                            <label for="check_password" class="col-form-label">{{ __('admin.base.check_password') }}:</label>
                            <input type="password" class="form-control" v-model="password_data.check_password">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __('admin.base.cancel') }}</button>
                    <button type="button" class="btn btn-primary" id="password_btn" @click="updatePassword" :disabled="passDisabled">{{ __('admin.base.submit') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('vue')
<script>
    const { createApp } = Vue
  
    var vm = createApp({
        data() {
            return {
                edit_data: {},
                password_data: {
                    new_password: '',
                    check_password: '',
                },
                isDisabled: false,
                passDisabled: false,
                answer: '',
            }
        },
        created() {
            this.getData();
        },
        methods: {
            getData: function () {
                var url = "{{ route('Admin.base.show') }}";
                axios.post(url)
                .then(function (response) {
                    vm.edit_data = response.data.configData;
                })
                .catch(function (error) {
                    this.answer = 'Error! Could not reach the API. ' + error;
                })
            },
            updateData: function () {
                vm.isDisabled = true;
                Swal.fire({
                    title: "Loading....",
                    allowOutsideClick: false,
                    timerProgressBar: true,
                    showConfirmButton: false,
                    allowEscapeKey: false,
                    timer: 20000,
                    didOpen: () => {
                        Swal.showLoading();
                    },
                });
                var update_url = "{{ route('Admin.base.update') }}";
                axios.post(update_url,{
                    name: vm.edit_data.name,
                })
                .then(function (response) {
                    vm.isDisabled = false;

                    if (response.data.type === false) {
                        swal.fire("Error!", response.data.message, "error");
                    } else {
                        swal.fire("OK!", response.data.message, "success");
                        vm.getData();
                    }
                })
                .catch(function (error) {
                    vm.answer = 'Error! Could not reach the API. ' + error;
                })
            },
            passwordData: function () {
                vm.password_data.new_password = '';
                vm.password_data.check_password = '';
            },
            updatePassword: function () {
                vm.passDisabled = true;
                $('#passwordModal').modal('hide');
                Swal.fire({
                    title: "Loading....",
                    allowOutsideClick: false,
                    timerProgressBar: true,
                    showConfirmButton: false,
                    allowEscapeKey: false,
                    timer: 20000,
                    didOpen: () => {
                        Swal.showLoading();
                    },
                });
                var update_password_url = "{{ route('Admin.base.password') }}";

                axios.post(update_password_url,{
                    new_password: vm.password_data.new_password,
                    check_password: vm.password_data.check_password,
                })
                .then(function (response) {
                    vm.passDisabled = false;

                    if (response.data.type === false) {
                        swal.fire("Error!", response.data.message, "error");
                    } else {
                        swal.fire("OK!", response.data.message, "success");
                    }
                    vm.password_data.new_password = '';
                    vm.password_data.check_password = '';
                    vm.getData();
                })
                .catch(function (error) {
                    vm.answer = 'Error! Could not reach the API. ' + error;
                })
            },
        }
    }).mount('#app')
</script>
@endsection