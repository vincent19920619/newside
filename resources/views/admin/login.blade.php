<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <title>@lang('admin.web')</title>
    <meta name="description" content="" />
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon/favicon.ico') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}" class="template-customizer-core-css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}" class="template-customizer-theme-css">
    <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/pages/page-auth.css') }}">
    <script src="{{ asset('assets/vendor/js/helpers.js') }}"></script>
    <script src="{{ asset('assets/js/config.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="container-xxl" id="app">
        <div class="authentication-wrapper authentication-basic container-p-y">
            <div class="authentication-inner">
                <div class="card">
                    <div class="card-body">
                        <div class="app-brand justify-content-center">
                            <span class="app-brand-text demo text-body fw-bolder">@lang('admin.web')</span>
                        </div>
                        <form id="formAuthentication" class="mb-3">
                            <div class="mb-3">
                                <input type="text" class="form-control" id="account" name="account" placeholder="@lang('admin.login.account')" v-model="login_data.account" autofocus/>
                            </div>
                            <div class="mb-3 form-password-toggle">
                                <div class="input-group input-group-merge">
                                    <input type="password" id="password" class="form-control" name="password" placeholder="@lang('admin.login.password')" v-model="login_data.password"/>
                                </div>
                            </div>
                            <div class="mb-3">
                                <button class="btn btn-primary d-grid w-100" id="btn1" type="button" @click="login()"  :disabled="isDisabled">@lang('admin.login.submit')</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/vendor/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/vendor/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('assets/vendor/js/menu.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('js/buttons.js') }}"></script>
    <script src="{{ asset('js/sweetalert2@11.js') }}"></script>
    <script src="{{ asset('js/vue.global.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script>
        const { createApp } = Vue
        var vm = createApp({
            data() {
                return {
                    login_data: {
                        account: '',
                        password: '',
                    },
                    isDisabled: false,
                }
            },
            methods: {
                login: function () {
                    vm.isDisabled = true;
                    var login_url = "{{ route('Admin.auth.check') }}";
                    axios.post(login_url,{
                        account: vm.login_data.account,
                        password: vm.login_data.password,
                        _token: $('meta[name="csrf-token"]').attr('content'),
                    })
                    .then(function (response) {
    
                        if (response.data.type === false) {
                            vm.isDisabled = false;
                            swal.fire("Error!", response.data.message, "error");
                        } else {
                            swal.fire("OK!", response.data.message, "success");
                            vm.login_data.account = '';
                            vm.login_data.password = '';
                            setTimeout(function(){
                                window.location.href = "{{ route('Admin.index') }}";
                            },2000);
                        }
                    })
                    .catch(function (error) {
                        vm.answer = 'Error! Could not reach the API. ' + error;
                    })
                },
            }
        }).mount('#app')
    </script>
</body>
</html>
