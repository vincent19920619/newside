@extends('admin.layouts.master')
@section('nav')
<h5 class="nav-header">@lang('admin.index.title')</h5>
@endsection
@section('content')
<div class="content-wrapper" id="app" v-cloak>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-lg-12 col-md-8 order-1">
                <div class="row">
                    <div class="col-lg-3 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <span class="fw-semibold d-block mb-1">{{ __('admin.index.member') }}</span>
                                <h3 class="card-title mb-2">@{{ member }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <span class="fw-semibold d-block mb-1">{{ __('admin.index.user') }}</span>
                                <h3 class="card-title mb-2">@{{ user }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <span class="fw-semibold d-block mb-1">{{ __('admin.index.message') }}</span>
                                <h3 class="card-title mb-2">@{{ message }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <span class="fw-semibold d-block mb-1">{{ __('admin.index.comment') }}</span>
                                <h3 class="card-title mb-2">@{{ comment }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-backdrop fade"></div>
</div>
@endsection
@section('vue')
<script>
    const { createApp } = Vue
    var vm = createApp({
        data() {
            return {
                url: "{{ route('Admin.index.show') }}",
                member: 0,
                user: 0,
                message: 0,
                comment: 0,
                answer: '',
            }
        },
        created() {
            this.getData();
        },
        methods: {
            getData: function () {
                axios.post(this.url)
                .then(function (response) {
                    vm.member = response.data.member;
                    vm.user = response.data.user;
                    vm.message = response.data.message;
                    vm.comment = response.data.comment;
                    console.log('a');
                })
                .catch(function (error) {
                    this.answer = 'Error! Could not reach the API. ' + error;
                })
            },
        }
    }).mount('#app')
</script>
@endsection