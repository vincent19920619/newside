<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"/>
    <title>@lang('admin.web')</title>
    <meta name="description" content="" />
    <link rel="icon" type="image/x-icon" href="{{ asset('assets/img/favicon/favicon.ico') }}">
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/boxicons.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/core.css') }}" class="template-customizer-core-css">
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-default.css') }}" class="template-customizer-theme-css">
    <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/css/pages/page-auth.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/lobibox.min.css') }}">
    <script src="{{ asset('assets/vendor/js/helpers.js') }}"></script>
    <script src="{{ asset('assets/js/config.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            <!-- Menu -->
            <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
                <div class="app-brand demo">
                    <a href="{!! route('Admin.index', [] ) !!}" class="app-brand-link">
                        <span class="app-brand-text demo menu-text fw-bolder ms-2">@lang('admin.web')</span>
                    </a>
                    <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
                        <i class="bx bx-chevron-left bx-sm align-middle"></i>
                    </a>
                </div>
                <div class="menu-inner-shadow"></div>
                <ul class="menu-inner py-1">
                    @foreach ($BASE_TPL as $item)
                        @if (empty($item['menu_list']))
                            <li class="menu-item{{ $item['active'] }}">
                                <a href="{{ $item['link'] }}" class="menu-link">
                                    <i class="{{ $item['icon'] }}"></i>
                                    <div data-i18n="Analytics">
                                        {{ $item['new_name']}}
                                    </div>
                                </a>
                            </li>
                        @else
                            <li class="menu-item{{ $item['active'] }}">
                                <a href="javascript:void(0);" class="menu-link menu-toggle">
                                    <i class="{{ $item['icon'] }}"></i>
                                    <div data-i18n="Layouts">
                                        {{ $item['new_name']}}
                                    </div>
                                </a>
                                @foreach ($item['menu_list'] as $val1)
                                <ul class="menu-sub">
                                    <li class="menu-item {{ $val1['active'] }}">
                                        <a href="{{ $val1['link'] }}" class="menu-link">
                                            <div data-i18n="Without menu">
                                                {{ $item['new_name']}}
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                @endforeach
                            </li>
                        @endif
                    @endforeach
                </ul>
            </aside>
            <!-- / Menu -->
            <div class="layout-page">
                <nav class="layout-navbar container-xxl navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
                    <div class="layout-menu-toggle navbar-nav align-items-xl-center me-3 me-xl-0 d-xl-none">
                        <a class="nav-item nav-link px-0 me-xl-4" href="javascript:void(0)">
                            <i class="bx bx-menu bx-sm"></i>
                        </a>
                    </div>
                    @yield('nav')
                    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
                        <ul class="navbar-nav flex-row align-items-center ms-auto">
                            <li class="nav-item navbar-dropdown dropdown-user dropdown">
                                <a class="nav-link dropdown-toggle hide-arrow" href="javascript:void(0);" data-bs-toggle="dropdown">
                                    {{ $NAME }}
                                </a>
                                <ul class="dropdown-menu dropdown-menu-end">
                                    <li>
                                        <a class="dropdown-item" href="{!! route('Admin.base', []) !!}">
                                            <i class="bx bx-world me-2"></i>
                                            <span class="align-middle">{{ __('admin.base.title') }}</span>
                                        </a>
                                    </li>
                                    {{-- <li>
                                        <a class="dropdown-item" href="{!! URL::route('Admin.change.lang', ['lang' => 'tw'] ) !!}">
                                            <i class="bx bx-world me-2"></i>
                                            <span class="align-middle">@lang('admin.language.tw')</span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="dropdown-divider"></div>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="{!! URL::route('Admin.change.lang', ['lang' => 'cn'] ) !!}">
                                            <i class="bx bx-world me-2"></i>
                                            <span class="align-middle">@lang('admin.language.cn')</span>
                                        </a>
                                    </li>
                                    <li>
                                        <div class="dropdown-divider"></div>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="{!! URL::route('Admin.change.lang', ['lang' => 'en'] ) !!}">
                                            <i class="bx bx-world me-2"></i>
                                            <span class="align-middle">@lang('admin.language.en')</span>
                                        </a>
                                    </li> --}}
                                    <li>
                                        <div class="dropdown-divider"></div>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="{!! route('Admin.auth.logout', [] ) !!}">
                                            <i class="bx bx-power-off me-2"></i>
                                            <span class="align-middle">{{ __('admin.logout') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- Content -->
                @yield('content')
                <!-- / Content -->
            </div>
        </div>
        <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <script src="{{ asset('assets/vendor/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('assets/vendor/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('assets/vendor/js/menu.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/dashboards-analytics.js') }}"></script>
    <script src="{{ asset('js/buttons.js') }}"></script>
    <script src="{{ asset('js/sweetalert2@11.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('js/Chart.js') }}"></script>
    <script src="{{ asset('js/lobibox.js') }}"></script>
    <script src="{{ asset('js/vue.global.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script src="{{ asset('js/vuejs-paginate-2.0.1.js') }}"></script>
    @yield('vue')
</body>
</html>
